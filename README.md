## GNN = GCN + GAN + GAE + GGN + GSTN

### GCN

#### 0. 写在前面

* 两种GCN = 基于空间的Spatial-based(直接用邻接矩阵乘 来聚合相邻节点的信息) + 基于频谱的Spectral-based(基于谱图理论 利用图傅立叶转化到谱空间 再做聚合)
* [空间_Spatial/vertex-based](https://www.lagou.com/lgeduarticle/73745.html#Neural_Network_for_Graphs_NN4G_19)
    * Neural Network for Graphs (NN4G)
    * Contextual Graph Markov Model (CGMM)
    * Diffusion Graph Convolution(DGC) 扩散图卷积
    * PGC-DGCNN
    * Partition Graph Convolution (PGC)
    * Message Passing Neural Networks (MPNN) 信息传递神经网络
    * Graph Isomorphism Network (GIN) 图同构网络
    * GraphSage
    * Graph Attention Network (GAT)图注意力网络
    * Gated Attention Network (GAAN)-门控注意力网络
    * Mixture Model Network (MoNet)
    * [PATCHY-SAN](https://github.com/tvayer/PSCN) = [Learning Convolutional Neural Networks for Graphs](http://proceedings.mlr.press/v48/niepert16.pdf)
    * Large-scale Graph Convolution Networks (LGCN)
* [频谱_Spectral-based](.) -- [Intro]()
    * <img src="https://www.zhihu.com/equation?tex=+y_%7Boutput%7D%3D%5Csigma+%5Cleft%28U+g_%5Ctheta%28%5CLambda%29+U%5ET+x+%5Cright%29+%5Cqquad%283%29" width=200>
    1) `GCNv1`: 基于(拉普拉斯矩阵L的特征值们)做图卷积操作 
        * [Spectral Networks and Locally Connected Networks on Graphs 2014](https://arxiv.org/abs/1312.6203)
        * <img src="https://www.zhihu.com/equation?tex=+g_%5Ctheta%28%5CLambda%29%3D%5Cleft%28%5Cbegin%7Bmatrix%7D%5Ctheta_1+%26%5C%5C%26%5Cddots+%5C%5C+%26%26%5Ctheta_n+%5Cend%7Bmatrix%7D%5Cright%29" width=200/>.
            * 卷积核g变成神经网络的参数来学习
        * [Code](src/GNN_with_Fast_Localized_Spectral_Filtering/lib/models.py#L387)
    2) `GCNv2`: 利用k介Chebyshev-polynomial做近似 
        * [Convolutional Neural Networks on Graphs with Fast Localized Spectral Filtering](https://arxiv.org/abs/1606.09375) (NIPS 2016)
        * <img src="https://www.zhihu.com/equation?tex=+g_%5Ctheta%28%5CLambda%29%3D%5Cleft%28%5Cbegin%7Bmatrix%7D%5Csum_%7Bj%3D0%7D%5EK+%5Calpha_j+%5Clambda%5Ej_1+%26%5C%5C%26%5Cddots+%5C%5C+%26%26+%5Csum_%7Bj%3D0%7D%5EK+%5Calpha_j+%5Clambda%5Ej_n+%5Cend%7Bmatrix%7D%5Cright%29" width=200>
        * [Code](src/GNN_with_Fast_Localized_Spectral_Filtering/lib/models.py#L488)
    3) `GCNv3`: 1stChebNet - 一阶近似ChebNet
        * ([Semi-Supervised Classification with Graph Convolutional Networks](http://arxiv.org/abs/1609.02907), 2016)

* <img src="https://img-blog.csdn.net/20171225185648115"  width=500/>

#### 1. Spectral-Domain 频域
* 思路
    1) 利用{傅立叶变换}频谱图形卷积(为了让邻居数量/顺序统一)
        * <img src="https://www.zhihu.com/equation?tex=%28f%2Ah%29_G%3DU%28%28U%5ETh%29%5Codot%28U%5ETf%29%29+%5Cqquad%282%29" width=200 />
    2) 用{拉普拉斯矩阵}来做{图的傅立叶变换}(不是巧合-通过量化{相邻顶点的特征差异}的公式推出来的)
        * <img src="https://www.zhihu.com/equation?tex=%5Csum_%7Bm%3D1%7D%5E%7Bk%7D+%5Csum_%7Bn%3D1%7D%5E%7Bk%7D+w_%7Bm+n%7D%5Cleft%28f_%7Bi+m%7D-f_%7Bi+n%7D%5Cright%29%5E%7B2%7D%5C%5C" width=200 />
* 缺点
    1) 对图的任何扰动都会导致特征基的变化 + 学习的过滤器依赖于不同领域 = 这意味着它们不能应用于具有不同结构的图 + 特征分解需要O(N^3)计算和O(N^2)内存
* 待解答
    * GCN为什么要利用Spectral graph theory
    * 拉普拉斯矩阵归一化的作用: (解决加权求和中)(某些靠大量邻居累积特征的节点)的问题 权值的和为1

#### 2. Spatial-Domain 空间域
* PATCHY-SAN = Learning Convolutional Neural Networks for Graphs
    * 整体思想 = 把graph搞成标准CNN可输入的样子 + 而不是GCN那样改CNN去适应graph
    * node-sequence选择(选择w个顶点当作卷积的中心) + neighbor-assembly(找到每个中心顶点receptive-field里面的点作为邻居) + neighbor-Norm(邻居节点**归一化**=确定序列号) = 每个卷积中心有等量的邻居作为输入 就可以和CNN一样做卷积了
    * 邻居节点归一化: 将目标node无序的neighbors映射为一个有序的vector
        * 难点: 对于两个不同的graphs 来自这两个graph的子结构g1和g2 它们在各自的graph中有相似的结构 那么他们label应该相似
        * 解法: 寻找一个一个labeling L 使得从图的集合中任意选取两个图G1和G2 它们在vector space距离差距和它们在graph space的距离差距最小化
    * <img src="https://pic1.zhimg.com/80/v2-89c94c41ca7868088bf5e515876bb1bc_hd.png" width=200>
    
* [解读三种经典GCN中的Parameter Sharing](https://zhuanlan.zhihu.com/p/72373094)
* [DGL 基于图的](https://docs.dgl.ai/tutorials/basics/1_first.html?highlight=gcn)

#### 3. Task = 节点分类[node] + 图分类[graph] + 边预测[link prediction] + 图的嵌入表示[graph embedding]
* [](https://www.zhihu.com/question/305395488/answer/554847680)


## Common Datase for GNN

### [Deep Graph Kernels (KDD 15)](http://www.doc88.com/p-7716904977863.html)
    * MUTAG (BioInfo)
    * PTC (BioInfo)
    * ENZYMES (BioInfo)
    * PROTEINS (BioInfo)
    * NCI1 (BioInfo)
    * NCI109 (BioInfo)
    * Reddit (Social)
    * COLLAB (Social)
    * IMDB (Social)

## Source Code


## Paper

### [powerful-gnns-master](?) ~ [How Powerful are Graph Neural Networks? (ICLR 2019)](?)
* 代码
    * 为什么用maxpooling
    * 

### [Semi-Supervised Classification with Graph Convolutional Networks (2016)](http://arxiv.org/abs/1609.02907)
* 改进:
    * 使用一阶近似简化计算的方法 提出了一种简单有效的层式传播方法 = 层状线性模型 
        * 把<img src="https://img-blog.csdn.net/20171225210856176" height="50"/>简化成<img src="https://img-blog.csdn.net/20171225214108704" height="45"/>
    * 思路是: 前人倾向于进行1次k-order的卷积(把距离中心节点小于k的node的信息 都卷积进来) vs (这里进行k次1-order的卷积)
    * 好处是: 可以充分挖掘(邻接矩阵中隐含的)(但没有在Node属性中表现的)关联信息 {比如文章引用=edge=文章的关联性/虽然文字上联系不强}
* [文章1](https://blog.csdn.net/yyl424525/article/details/98724104) - [文章2](https://zhuanlan.zhihu.com/p/78624225)
* 代码分析 [gcn-master]()
    * 图卷积层的基本操作
    ```
    GraphConvolution(Layer):  # ReLU(AXW)
        dropout = Dropout(dropout_rate, seed=seed)
        kernel  = add_weight(shape=(input_dim, units), initializer=glorot_uniform(), regularizer=l2(l2_reg), name='kernel', )
        bias    = add_weight(shape=(units,), initializer=Zeros(), name='bias', )
        output  = tf.matmul(tf.sparse_tensor_dense_matmul(A, features), kernel) + bias
        act     = activation(output)

    def GCN():
        Adj   = Input(shape=(None,), sparse=True)
        X_in  = Input(shape=(feature_dim,), )
        for i in range(num_layers):
            h = GraphConvolution(n_hidden, activation=activation, dropout_rate=dropout_rate, l2_reg=l2_reg)([h,Adj])
        output = h
        model = Model(inputs=[X_in,Adj], outputs=output)
        return model
    ```

    * Normalize Adj Matrix
    ```
    def preprocess_features(features):
        """Row-normalize feature matrix and convert to tuple representation"""
        rowsum = np.array(features.sum(1))
        r_inv = np.power(rowsum, -1).flatten()
        r_inv[np.isinf(r_inv)] = 0.
        r_mat_inv = sp.diags(r_inv)
        features = r_mat_inv.dot(features)
        return features.todense()
    ```
* 实验
    * DataSet: 
        * Citation Network: `Cora`, `Citeseer` or `PubMed`
        * Knowledge Graph: `NELL`
        * From [Revisiting Semi-Supervised Learning with Graph Embeddings](https://arxiv.org/abs/1603.08861), ICML 2016
    * 结果
        * <img src="https://img-blog.csdn.net/20171225222332505" width=444 />
        * <img src="https://note.youdao.com/yws/api/personal/file/WEB47d38f4ff5e52ae2cf8e4bcd47bd8edd?method=download&shareKey=b39416c911203c6c1879159b0967f0ac" width=444 />
