## How Powerful are Graph Neural Networks? (基于PyTorch的实现)

This repository is the official PyTorch implementation of the experiments in the following paper: 
Keyulu Xu*, Weihua Hu*, Jure Leskovec, Stefanie Jegelka. How Powerful are Graph Neural Networks? ICLR 2019. 
[arXiv](https://arxiv.org/abs/1810.00826) [OpenReview](https://openreview.net/forum?id=ryGs6iA5Km) 

### **代码利用GCN+Pooling对整个图进行分类**

## Test run
Unzip the dataset file
```
pip install -r requirements.txt

unzip dataset.zip
python main.py
```

## 分析
* Load Data
    * Raw Data格式
    ```
    20(条记录) 0(标签) <--其中一个图的信息
    0 5 (edge的个数) 2 4 5 9 10 (edge的另一个顶点) [当前顶点为这个raw的index]
    0 8 (edge的个数) 2 6 8 12 14 17 18 19
    ... ...
    0 12(edge的个数) 1 2 3 6 8 11 12 14 15 16 17 18
    ```
    * Parse格式
        * 数据按照格式被整理成【g=nx.Graph()】的形式 = 即NX包的结构
        * node_features = 每个node的degree数 转化成1hot-embedding的形式
        * neighbors = 邻接矩阵

* Layers
    * GCN
    * Pooling - 文中提到是为了提取图中的general特征(因为任务是graph-level的分类)
        * 