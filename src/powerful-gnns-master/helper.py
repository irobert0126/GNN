import torch
from torch import nn
from torchviz import make_dot
from models.graphcnn import GraphCNN

model = nn.Sequential()
model.add_module('W0', nn.Linear(8, 16))
model.add_module('tanh', nn.Tanh())
model.add_module('W1', nn.Linear(16, 1))

model = GraphCNN(args.num_layers, args.num_mlp_layers, train_graphs[0].node_features.shape[1],
                 args.hidden_dim, num_classes, args.final_dropout, args.learn_eps, args.graph_pooling_type, args.neighbor_pooling_type, device).to(device)


x = torch.randn(1,8)

vis_graph = make_dot(model(x), params=dict(model.named_parameters()))
vis_graph.view()